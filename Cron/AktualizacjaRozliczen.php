<?php


namespace Kowal\APIwFirmaRozliczenia\Cron;

class AktualizacjaRozliczen
{

    protected $logger;

    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $this->logger->addInfo("Cronjob AktualizacjaRozliczen is executed.");
    }
}
