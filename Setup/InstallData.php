<?php


namespace Kowal\APIwFirmaRozliczenia\Setup;

use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\InstallDataInterface;

class InstallData implements InstallDataInterface
{

    private $salesSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Sales\Setup\SalesSetupFactory $salesSetupFactory
     */
    public function __construct(SalesSetupFactory $salesSetupFactory)
    {
        $this->salesSetupFactory = $salesSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
        $salesSetup->addAttribute('order', 'id_faktury_wfirma',
            [
                'type' => 'varchar',
                'label' => 'ID faktury wFirma',
                'length' => 255,
                'visible' => false,
                'required' => false,
                'grid' => true
            ]
        );

        $salesSetup->addAttribute('order', 'rozliczono',
            [
                'type' => 'varchar',
                'label' => 'Rozliczono',
                'length' => 255,
                'visible' => false,
                'required' => false,
                'grid' => true
            ]
        );

        $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
        $salesSetup->addAttribute('order', 'status_rozliczenia',
            [
                'type' => 'varchar',
                'label' => 'Status Rozliczenia',
                'length' => 255,
                'visible' => false,
                'required' => false,
                'grid' => true
            ]
        );
    }
}
